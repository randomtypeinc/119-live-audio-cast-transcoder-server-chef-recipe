Overview
========

This repository contains the necessary steps to install the codem-transcode
nodejs application on an Ubuntu 12.04 server.

Bootstrap (prepare server)
==========================

First step is to bootstrap your server. Ubuntu 12.04 comes with an older version of Ruby that doesn't play nice with recent Chef and Chef cookbooks.

The bootstrap script is executed from your workstation pointing to the remote server and executes commands over SSH utilizing the given private key. The following steps are provided by the bootstrap script:

* Execute an aptitude update
* Check for and upgrade Ruby version to 1.9.3 (as provided in the apt repository).
* Check for and upgrade/install Chef (latest version is installed via omnibus installer).
* Install git version control
* Clone the Chef codem-transcode repository onto remote server
* Check for and create **codem** user account on the remote server (this account is used to run the codem-transcode application).

**Ensure the _codem_ user at the top of the bootstrap script and in the _node.json_ file match.**

The parameters to the bootstrap script are the same as provided the ssh on the command line (ie: -p 2222 for a different port).
```
./ubuntu_12.04_bootstrap.sh -i server_private_key root@myserver
```

The bootstrap script with ask a series of questions:

* Enter a username to create on the server that will execute the codem-transcode application (default: **codem**)
* Enter a password for the codem-transcode new user (default: **Ch4ngem3!**)
* Enter the username used for the GIT repository for the codem-transcode application (REQUIRED!)
* Enter the password for the GIT repository for the codem-transcode application (REQUIRED!)
* Finally, after the preparing steps, it will ask to execute the Chef recipe (the answer must be a capital Y to start, otherwise - it will exit with instructions)

Execute Chef recipe
===================

**If you choose N from the bootstrap script, the following commands must be executed from the remote server**

Now that you should have the Chef codem-transcode repository cloned onto the remote server. You can now log into the remote server and execute the Chef installer(via **chef-solo**).

**Please note these instructions are _only_ for _chef-solo_ and not for full installations of Chef utilizing the hosted Chef server for cookbooks/etc.**

Executing the Chef recipe:
```
cd ~/chef-codem-transcode
sudo chef-solo -c solo.rb -j node.json
```

The repository contains all the required dependent cookbooks (apt, git, nginx, mysql, etc...).
