#!/bin/bash

# bootstrap.sh - Script to bootstrap an Ubuntu 12.04 server with
#                pre-requisites for the codem-transcode Chef recipe.
#                Ensure the user being used for SSH access has sudo
#                NOPASSWD in the sudoers config.
#                Also ensure that you update the node['config']['user']
#                entry in the node.json file with the correct username.

# Requirements:
#
#   Ruby ~= 1.9
#   Chef ~= 12
echo Preparing remote server for codem-transcode...

# Bootstrap steps performed:
#  1. Perform "aptitude update" to refresh package lists
#  2. Upgrade Ruby version (Ubuntu 12.04 has the ruby1.9.3 package)
#  3. Install Chef latest version (required for chef-solo)
#  4. Install git client
#  5. Create "codem" user

# Assign user for codem-transcode application
echo -n "What username do you want to run codem-transcode [codem]:"
read CODEM_USER
if [[ $CODEM_USER == "" ]]; then
    CODEM_USER="codem"
fi
echo -n "Enter a password to use when creating ${CODEM_USER} [Ch4ngem3!]:"
read -s CODEM_PASSWD
echo
if [[ $CODEM_PASSWD == "" ]]; then
    CODEM_PASSWD="Ch4ngem3!"
fi

CODEM_REPO_URL="https://bitbucket.org/rti-kinnectall/119-live-audio-cast-transcoder-application"
echo "Using the [$CODEM_REPO_URL] repository for codem-transcode application"
echo "Enter the credentials:"
echo -n "username:"
read REPO_USER
if [[ $REPO_USER == "" ]]; then
    echo -n "Are you sure you don't need a username [y/N]?"
    read ANSWER
    if [[ $ANSWER != "Y" ]]; then
        echo "Exiting, please try again with a valid username."
        exit
    fi
fi
REPO_PASSWD=""
if [[ $REPO_USER != "" ]]; then
    echo -n "password:"
    read -s REPO_PASSWD
    echo
    if [[ $REPO_PASSWD == "" ]]; then
        echo "A password must be supplied, please try again."
        exit
    fi
fi

# choose a password for mysql root and codem-transcode application
echo "Please enter a password for the MySQL root user account:"
echo -n "MySQL root password [Ch4ngem3!]:"
read -s MYSQL_ROOT
echo
if [[ $MYSQL_ROOT == "" ]]; then
    MYSQL_ROOT="Ch4ngem3!"
fi

# rebuild the repository URL with credentials
CODEM_REPO_URL="https://${REPO_USER}:${REPO_PASSWD}@bitbucket.org/rti-kinnectall/119-live-audio-cast-transcoder-application"

# Generate heredoc for bootstrap script
read -r -d '' BOOTSTRAP_CMDS <<-'EOF'
#!/bin/bash
#
# Retrieve user to create and execute codem-transcode application
CODEM_USER=$1
CODEM_PASSWD=$2
CODEM_REPO_URL=$3
MYSQL_ROOT=$4

# refresh repository
sudo aptitude update

# Check for and install Ruby v1.9
[[ $(which ruby) == "" ]] && RUBY_VERSION="UNKNOWN" || RUBY_VERSION=$(ruby -v)
if [[ $RUBY_VERSION != *"1.9."* ]]; then
    echo Instaling/upgrading Ruby, detected: [${RUBY_VERSION}]
    sudo aptitude install -q -y ruby1.9.3
    sudo update-alternatives --install /usr/bin/ruby ruby /usr/bin/ruby1.9.1 400 \
        --slave   /usr/share/man/man1/ruby.1.gz ruby.1.gz \
                  /usr/share/man/man1/ruby1.9.1.1.gz \
        --slave   /usr/bin/ri ri /usr/bin/ri1.9.1 \
        --slave   /usr/bin/irb irb /usr/bin/irb1.9.1 \
        --slave   /usr/bin/rdoc rdoc /usr/bin/rdoc1.9.1
else
    echo Existing Ruby version found: $RUBY_VERSION
fi

# Check for and install Chef
[[ $(which chef-solo) == "" ]] && CHEF_VERSION="UNKNOWN" || CHEF_VERSION=$(chef-solo -v)
if [[ $CHEF_VERSION != *"12."* ]]; then
    echo Installing/upgrading Chef, detected [$CHEF_VERSION]
    wget -q -O - https://www.opscode.com/chef/install.sh | sudo bash -s -- -v 12
else
    echo Existing Chef version: $CHEF_VERSION
fi

# Check for and install git
GIT_CMD=$(which git)
if [[ $GIT_CMD == "" ]]; then
    echo Installing git...
    sudo aptitude install -q -y git
else
    echo Git already installed in: $GIT_CMD
fi

# clone repository
if [[ -d ~/chef-codem-transcode ]]; then
    echo Existing chef-codem-transcode repository found, removing...
    rm -rf ~/chef-codem-transcode
fi
echo Cloning chef-codem-transcode repository...
git clone https://mbondfusion@bitbucket.org/mbondfusion/chef-codem-transcode.git ~/chef-codem-transcode

# creating codem user
USER_EXISTS=$(grep ${CODEM_USER} /etc/passwd)
if [[ $USER_EXISTS == "" ]]; then
    echo Creating codem-transcode user: $CODEM_USER
    sudo useradd -c 'Automation user for codem-transcode application' \
        -d /home/${CODEM_USER} -m -p ${CODEM_PASSWD} -s /bin/bash ${CODEM_USER}
else
    echo User already exists: $CODEM_USER
fi

# update the node.json file with:
#  - local "codem" user account
#  - codem-transcode repository URL
#  - mysql root password
echo Updating node.json file with configuration changes...
cd ~/chef-codem-transcode
sed -i "s/\"user\": .*$/\"user\": \"$CODEM_USER\"/g" node.json
sed -i "s<\"giturl\": .*$<\"giturl\": \"$CODEM_REPO_URL\",<g" node.json

# three fields for mysql password
sed -i "s/\"server_root_password\": .*$/\"server_root_password\": \"$MYSQL_ROOT\",/g" node.json
sed -i "s/\"server_debian_password\": .*$/\"server_debian_password\": \"$MYSQL_ROOT\",/g" node.json
sed -i "s/\"server_repl_password\": .*$/\"server_repl_password\": \"$MYSQL_ROOT\"/g" node.json

EOF

# Save the bootstrap script remotely
echo "$BOOTSTRAP_CMDS" | ssh $@ "cat > /tmp/bootstrap.sh"

# Execute the remote script from /tmp/bootstrap.sh
ssh $@ "bash /tmp/bootstrap.sh ${CODEM_USER} ${CODEM_PASSWD} ${CODEM_REPO_URL} ${MYSQL_ROOT}"

# Execute Chef recipe for codem-transcode
echo -n "Do you want to execute the Chef recipe now (must be capital 'Y' to continue) [N]?"
read ANSWER
if [[ $ANSWER == "Y" ]]; then
    ssh $@ "cd ~/chef-codem-transcode && sudo chef-solo -c solo.rb -j node.json"
else
    echo "Be sure to execute the Chef recipe on the remote server"
    echo "with the following command:"
    echo
    echo "    cd ~/chef-codem-transcode"
    echo "    sudo chef-solo -c solo.rb -j node.json"
fi
